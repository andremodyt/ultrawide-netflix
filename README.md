# Ultrawide Netflix

Mozilla Firefox extension for Netflix. It allows to fulfill a 21:9 screen when watching 21:9 recorded content (Normally movies).

You can download it from AMO: https://addons.mozilla.org/firefox/addon/ultrawide-netflix/

## Credits

Thanks to @InfInt for the dynamic offset calculation of video margins
